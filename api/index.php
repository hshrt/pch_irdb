<?php
/**
 * Created by PhpStorm.
 * User: johnau
 * Date: 10/26/2017
 * Time: 5:05 PM
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require '../db.inc';

$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true]]);


function generate_error_response($msg) {
    $response = [];
    $response['response'] = 'error';
    $response['data'] = [];
    $response['msg'] = $msg;
    return $response;
}

//For debug
$app->options('/radio', function (Request $request, Response $response) {
    $response = $response->withHeader('Access-Control-Allow-Origin', '*');
    $response = $response->withHeader('Access-Control-Allow-Headers', 'Content-Type');
    return $response;
});


//Get all radio
$app->get('/radio/', function (Request $request, Response $response) {
    $list = [];
    $queryParams = $request->getQueryParams();
    $page = null;
    $pageSize = null;
    $searchKeyword = null;
    $isEnable = null;
    $isOnline = null;
    if (array_key_exists('page', $queryParams) && array_key_exists('per_page', $queryParams)) {
        if (is_numeric($queryParams['page']) && is_numeric($queryParams['per_page'])) {
            $page = $queryParams['page'];
            $pageSize = $queryParams['per_page'];
        }
    }
    if (array_key_exists('search', $queryParams)) {
        $searchKeyword = $queryParams['search'];
    }
    if (array_key_exists('isEnable', $queryParams)) {
        error_log('isEnable!');
        $isEnable = $queryParams['isEnable'];
    }
    if (array_key_exists('isOnline', $queryParams)) {
        $isOnline = $queryParams['isOnline'];
    }
    $list = db_get_all_radio($page, $pageSize, $searchKeyword, $isEnable, $isOnline);
    $response = $response->withHeader('Access-Control-Allow-Origin', '*');
    $response = $response->withHeader('Access-Control-Allow-Headers', 'Content-Type');
    $response->getBody()->write(json_encode($list));
    return $response;
});

//Get single radio
$app->get('/radio/{id}', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');
    $list = db_get_radio_by_id($id);
    $response = $response->withHeader('Access-Control-Allow-Origin', '*');
    $response = $response->withHeader('Access-Control-Allow-Headers', 'Content-Type');
    $response->getBody()->write(json_encode($list));
    return $response;
});

//Create radio
$app->post('/radio', function (Request $request, Response $response) {
    $parsedBody = $request->getParsedBody();
    $myGenre = $parsedBody['myGenre'];
});

function validate_radio_input($input) {
    if (!array_key_exists('radioName', $input) || empty($input['radioName'])) {
        return array(false, 'Radio Name required');
    }
    if (!array_key_exists('radioURL', $input) || empty($input['radioURL'])) {
        return array(false, 'URL required');
    }
    if (!array_key_exists('myGenre', $input) || empty($input['myGenre'])) {
        return array(false, 'Genre required');
    }
    if (!array_key_exists('myCountry', $input) || empty($input['myCountry'])) {
        return array(false, 'Country required');
    }
    if (!array_key_exists('myCity', $input) || empty($input['myCity'])) {
        return array(false, 'City required');
    }
    if (!array_key_exists('radioFormat', $input) || empty($input['radioFormat'])) {
        return array(false, 'Format required');
    }
    if (!array_key_exists('myBitrate', $input) || empty($input['myBitrate'])) {
        return array(false, 'Bitrate required');
    }
    return array(true, 'ok');
}

//Create radio
$app->put('/radio/', function (Request $request, Response $response) {
    $parsedBody = $request->getParsedBody();

    $myID = $request->getAttribute('id');
    $responseCode = 200;
    $result = [];
    $validate_result = validate_radio_input($parsedBody);
    if (!$validate_result[0]) {
        $result = generate_error_response($validate_result[1]);
        $responseCode = 500;
    } else {
        $radioName = $parsedBody['radioName'];
        $radioURL = $parsedBody['radioURL'];
        $myGenre = $parsedBody['myGenre'];
        $myCountry = $parsedBody['myCountry'];
        $myCity = $parsedBody['myCity'];
        $radioFormat = $parsedBody['radioFormat'];
        $myBitrate = $parsedBody['myBitrate'];

        $result = db_insert_radio($radioName, $radioURL, $myGenre, $myCountry, $myCity, $radioFormat, $myBitrate);
        if ($result['response'] == 'error') {
            $responseCode = 500;
        }
    }

    $response = $response->withStatus($responseCode);
    $response->getBody()->write(json_encode($result));
    return $response;
});

//Update radio
$app->put('/radio/{id}', function (Request $request, Response $response) {
    $parsedBody = $request->getParsedBody();
    $myID = $request->getAttribute('id');
    $radioName = $parsedBody['radioName'];
    $radioURL = $parsedBody['radioURL'];
    $myGenre = $parsedBody['myGenre'];
    $myCountry = $parsedBody['myCountry'];
    $myCity = $parsedBody['myCity'];
    $radioFormat = $parsedBody['radioFormat'];
    $myBitrate = $parsedBody['myBitrate'];
    $isEnable = $parsedBody['isEnable'];
    $responseCode = 200;
    $result = [];
    if (empty($radioName)) {
        $result = generate_error_response('Radio Name required');
        $responseCode = 500;
    }
    if (empty($radioURL)) {
        $result = generate_error_response('URL required');
        $responseCode = 500;
    }
    if (empty($myGenre)) {
        $result = generate_error_response('Genre required');
        $responseCode = 500;
    }
    if (empty($myCountry)) {
        $result = generate_error_response('Country required');
        $responseCode = 500;
    }
    if (empty($myCity)) {
        $result = generate_error_response('City required');
        $responseCode = 500;
    }
    if (empty($radioFormat)) {
        $result = generate_error_response('Format required');
        $responseCode = 500;
    }
    if (empty($myBitrate)) {
        $result = generate_error_response('Bitrate required');
        $responseCode = 500;
    }
    error_log($result);
    if ($responseCode == 200) {
        $result = db_update_radio($myID, $radioName, $radioURL, $myGenre, $myCountry, $myCity, $radioFormat, $myBitrate, $isEnable);
        if ($result['response'] == 'error') {
            $responseCode = 500;
        }
    }
    $response = $response->withStatus($responseCode);
    $response->getBody()->write(json_encode($result));
    return $response;
});

//Delete radio
$app->delete('/radio/{id}', function (Request $request, Response $response) {
    $responseCode = 200;
    $myID = $request->getAttribute('id');
    if (empty($myID) || !is_numeric($myID)) {
        $result = generate_error_response('Invalid myID');
        $responseCode = 500;
    } else {
        $result = db_delete_radio($myID);
        if ($result['response'] == 'error') {
            $responseCode = 500;
        }
    }
    $response = $response->withStatus($responseCode);
    $response->getBody()->write(json_encode($result));
    return $response;
});

//Get all preset
$app->get('/preset/', function (Request $request, Response $response) {
    $list = db_get_all_preset();
    $response = $response->withHeader('Access-Control-Allow-Origin', '*');
    $response = $response->withHeader('Access-Control-Allow-Headers', 'Content-Type');
    $response->getBody()->write(json_encode($list));
    return $response;
});

//Get single preset
$app->get('/preset/{id}', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');
    $list = db_get_preset_by_id($id);
    $response = $response->withHeader('Access-Control-Allow-Origin', '*');
    $response = $response->withHeader('Access-Control-Allow-Headers', 'Content-Type');
    $response->getBody()->write(json_encode($list));
    return $response;
});

//Update preset
$app->put('/preset/{id}', function (Request $request, Response $response) {
    $parsedBody = $request->getParsedBody();
    $id = $request->getAttribute('id');
    $name = $parsedBody['name'];
    $url = $parsedBody['url'];
    $responseCode = 200;
    $result = [];
    if (empty($name)) {
        $result = generate_error_response('Radio Name required');
        $responseCode = 500;
    }
    if (empty($url)) {
        $result = generate_error_response('URL required');
        $responseCode = 500;
    }
    error_log($result);
    if ($responseCode == 200) {
        $result = db_update_preset($id, $name, $url);
        if ($result['response'] == 'error') {
            $responseCode = 500;
        }
    }
    $response = $response->withStatus($responseCode);
    $response->getBody()->write(json_encode($result));
    return $response;
});

//Export radio
$app->get('/export/main', function (Request $request, Response $response) {
    $regen_cache = false;
    $result = db_get_last_update('radio');
    if (!file_exists('/tmp/hsh_list.last_udate_radio')) {
        $regen_cache = true;
    } else {
        $old_last_update = file_get_contents('/tmp/hsh_list.last_udate_radio');
        if ($old_last_update != $result['data'][0]->last_update) {
            $regen_cache = true;
        }
    }

    if ($regen_cache) {
        $last_update_file = fopen('/tmp/hsh_list.last_udate_radio', 'w');
        fwrite($last_update_file, $result['data'][0]->last_update);

        if (!file_exists('/tmp/hsh_list.lock')) {
            error_log('I\'m regening cache!');
            $lock_file = fopen('/tmp/hsh_list.lock', 'w');
            fwrite($lock_file, 'lock');
            fclose($lock_file);
            gen_export_main();
            unlink('/tmp/hsh_list.lock');
        }
    }

    while (file_exists('/tmp/hsh_list.lock')) {
        error_log('Waiting for cache finish...');
        sleep(1);
    }

    return file_get_contents('/tmp/hsh_list.tsv');
});

function gen_export_main() {
    // Location
    $file = fopen('/tmp/hsh_list.tsv', 'w');
    $response = "";
    $result = db_get_radio_sort_by_location();
    foreach($result['data'] as $row) {
        $line = [];
        $radioName = iconv("UTF-8", "ASCII//TRANSLIT", $row->radioName);
        if (strlen($radioName) > 40) {
            $radioName = substr($radioName, 0, 37) . '...';
        }
        $line[] = 'Location';
        $line[] = $row->myCountry;
        $line[] = $radioName;
        $line[] = $row->radioURL;
        $line[] = $row->myBitrate;
        $line[] = $row->radioFormat;
        $line[] = substr(str_replace('@', '/', $row->myGenre), 1, -1);
        $line[] = $row->myCity;
        fwrite($file, implode("\t", $line) . "\n");
    }

    // Pre-Genre
    $allGenres = [];
    $result = db_get_radio_genre();
    foreach($result['data'] as $row) {
        $genres = explode("@", $row->myGenre);
        foreach($genres as $g) {
            if (!empty($g) && !in_array($g, $allGenres)) {
                $allGenres[] = $g;
            }
        }
    }

    asort($allGenres);
    foreach($allGenres as $g) {
        $result = db_get_radio_by_genre($g);
        foreach ($result['data'] as $row) {
            $line = [];
            $radioName = iconv("UTF-8", "ASCII//TRANSLIT", $row->radioName);
            if (strlen($radioName) > 40) {
                $radioName = substr($radioName, 0, 37) . '...';
            }
            $line[] = 'Genre';
            $line[] = $g;
            $line[] = $radioName;
            $line[] = $row->radioURL;
            $line[] = $row->myBitrate;
            $line[] = $row->radioFormat;
            fwrite($file,implode("\t", $line) . "\n");
        }
    }

    // Genre

    fclose($file);

}

//Export preset
$app->get('/export/preset', function (Request $request, Response $response) {

    $result = db_get_all_preset();
    foreach ($result['data'] as $row) {
        $line = [];
        $line[] = $row->name;
        $line[] = $row->url;
        $line[] = 'MP3';
        $response->getBody()->write(implode("\t", $line) . "\n");
    }

    return $response;

});

$app->get('/last_update/{type}', function (Request $request, Response $response) {
    $type = $request->getAttribute('type');
    if ($type != 'radio' && $type != 'preset') {
        $response->getBody()->write(json_encode(generate_error_response('Invalid input')));
    } else {
        $result = db_get_last_update($type);
        $response->getBody()->write($result['data'][0]->last_update . "\n");
    }

    return $response;
});

$app->get('/tester_status', function (Request $request, Response $response) {
    $result = db_get_test_count();
    $response->getBody()->write(json_encode($result));
    return $response;
});

$app->get('/disable_offline', function (Request $request, Response $response) {
    $queryParams = $request->getQueryParams();
    if (!array_key_exists('count', $queryParams) || !is_numeric($queryParams['count'])) {
        $response->getBody()->write(json_encode(generate_error_response('Invalid input')));
    } else {
        $result = db_disable_offline_radio((int)$queryParams['count']);
        $response->getBody()->write(json_encode($result));
    }
});

$app->get('/tester/pause', function (Request $request, Response $response) {
    $result = db_get_tester_config_by_key('status');
    if ($result['data'][0]->value == 'RUNNING') {
        $result = db_toggle_tester_pause();
    } else {
        $result = db_get_tester_config_by_key('command');
        if ($result['data'][0]->value == 'PAUSE') {
            $result = db_toggle_tester_pause();
        } else {
            $result = generate_error_response('Not Running');
        }
    }
    $response->getBody()->write(json_encode($result));
    return $response;
});

$app->get('/tester/start', function (Request $request, Response $response) {
    $result = db_start_tester();
    $response->getBody()->write(json_encode($result));
    return $response;
});

$app->get('/tester/stop', function (Request $request, Response $response) {
    $result = db_stop_tester();
    $response->getBody()->write(json_encode($result));
    return $response;
});

$app->get('/tester_config', function (Request $request, Response $response) {
    $result = db_get_tester_config();
    $response->getBody()->write(json_encode($result));
    return $response;
});

$app->put('/tester_config', function (Request $request, Response $response) {
    $config = json_decode($request->getBody(), true);
    if (isset($config)) {
        $result = db_set_tester_config($config);
        $response->getBody()->write(json_encode($result));
    } else {
        $response->getBody()->write(json_encode(generate_error_response('Invalid input')));
    }
    return $response;
    //$result = db_set_tester_config($config);
});

$app->run();

?>