<?php
/**
 * Created by PhpStorm.
 * User: johnau
 * Date: 10/26/2017
 * Time: 5:24 PM
 */
include_once('config.inc');

function db_connect() {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    return $conn;
}

function db_execute_sql($sql, $param, $paramType, $countRowSql = null, $countRowParam = null, $countRowParamType = null) {
    $conn = db_connect();
    $p = [];
    foreach($param as $key => $value) {
        $p[$key] = $value;
    }
    try {
        $stmt = $conn->prepare($sql);
        foreach($p as $key => $value) {
            if (array_key_exists($key, $paramType)) {
                $p[$key] = intval($p[$key]);
                $stmt->bindParam($key, $p[$key], PDO::PARAM_INT);
            } else {
                $stmt->bindParam($key, $p[$key]);
            }
        }
        if ($stmt->execute()) {
            $result['response'] = 'ok';
        } else {
            $result['response'] = 'error';
            $result['msg'] = implode(" :: ", $stmt->errorInfo());
            return $result;
        }
        $resultset = $stmt->fetchAll(PDO::FETCH_OBJ);
        $stmt->closeCursor();
        $result['response'] = 'ok';
        $result['data'] = $resultset;

        if (isset($countRowSql)) {
            $stmt = $conn->prepare($countRowSql);
            $p = [];
            foreach($countRowParam as $key => $value) {
                $p[$key] = $value;
            }
            foreach($p as $key => $value) {
                if (array_key_exists($key, $countRowParamType)) {
                    $p[$key] = intval($p[$key]);
                    $stmt->bindParam($key, $p[$key], PDO::PARAM_INT);
                } else {
                    $stmt->bindParam($key, $p[$key]);
                }
            }
            $stmt->execute();
            $rowcount = $stmt->fetchAll(PDO::FETCH_NUM);
            $result['meta'] = array('row_count' => $rowcount[0][0]);
        }

        return $result;
    } catch(PDOException $e) {
        $result['response'] = 'error';
        $result['msg'] = $e->getMessage();
        $result['data'] = [];
        error_log($e->getMessage());
        return $result;
    }
}

function db_execute_update($sql, $param, $paramType) {
    $conn = db_connect();
    $p = [];
    foreach($param as $key => $value) {
        $p[$key] = $value;
    }
    try {
        $stmt = $conn->prepare($sql);
        foreach($p as $key => $value) {
            if (array_key_exists($key, $paramType)) {
                if ($paramType[$key] == PDO::PARAM_NULL) {
                    $p[$key] = null;
                    $stmt->bindParam($key, $p[$key], PDO::PARAM_NULL);
                } else {
                    $stmt->bindParam($key, intval($p[$key]), PDO::PARAM_INT);
                }
            } else {
                $stmt->bindParam($key, $p[$key]);
            }
        }
        if ($stmt->execute()) {
            $result['response'] = 'ok';
        } else {
            $result['response'] = 'error';
            $result['msg'] = implode(" :: ", $stmt->errorInfo());
        }
        return $result;
    } catch(PDOException $e) {
        $result['response'] = 'error';
        $result['msg'] = $e->getMessage();
        error_log($e->getMessage());
        return $result;
    }
}

function db_get_user_by_username($username) {
    return db_execute_sql("SELECT * FROM user WHERE username = :username", array("username" => $username), array());
}

function db_get_user_by_id($id) {
    return db_execute_sql("SELECT * FROM user WHERE id = :id", array("id" => $id), array());
}

function db_get_all_radio($page = null, $pageSize = null, $searchKeyword = null, $isEnable = null, $isOnline = null) {

    $param = array();
    $paramType = array();
    $countRowParam = array();
    $countRowParamType = array();
    $sql = 'select * from `Radio`.`myradio` WHERE 1=1';
    $countRowSql = 'select COUNT(*) from `Radio`.`myradio` WHERE 1=1';

    if (isset($searchKeyword)) {
        $append = ' AND (UPPER(radioName) LIKE :search ' .
            'OR UPPER(radioURL) LIKE :search1 ' .
            'OR UPPER(myGenre) LIKE :search2 ' .
            'OR UPPER(myCountry) LIKE :search3 ' .
            'OR UPPER(myCity) LIKE :search4)';
        $sql .= $append;
        $countRowSql .= $append;
        $param['search'] = '%' . strtoupper($searchKeyword) . '%';
        $countRowParam['search'] = '%' . strtoupper($searchKeyword) . '%';
        $param['search1'] = '%' . strtoupper($searchKeyword) . '%';
        $countRowParam['search1'] = '%' . strtoupper($searchKeyword) . '%';
        $param['search2'] = '%' . strtoupper($searchKeyword) . '%';
        $countRowParam['search2'] = '%' . strtoupper($searchKeyword) . '%';
        $param['search3'] = '%' . strtoupper($searchKeyword) . '%';
        $countRowParam['search3'] = '%' . strtoupper($searchKeyword) . '%';
        $param['search4'] = '%' . strtoupper($searchKeyword) . '%';
        $countRowParam['search4'] = '%' . strtoupper($searchKeyword) . '%';
    }
    if (isset($isEnable) && ($isEnable == '0' || $isEnable == '1')) {
        $append = ' AND isEnable = :isEnable';
        $sql .= $append;
        $countRowSql .= $append;
        $param['isEnable'] = $isEnable;
        $countRowParam['isEnable'] = $isEnable;
    }
    if (isset($isOnline) && ($isOnline == '0' || $isOnline == '1')) {
        $append = ' AND offlinecount ' . ($isOnline == '1' ? ' = ' : ' > ') . '0';
        $sql .= $append;
        $countRowSql .= $append;
    }

    $sql .= " ORDER BY radioName ";
    if (isset($page) && isset($pageSize)) {
        $sql .= ' LIMIT :offset, :row_count';
        $param['offset'] = intval($page) * intval($pageSize);
        $paramType['offset'] = PDO::PARAM_INT;
        $param['row_count'] = intval($pageSize);
        $paramType['row_count'] = PDO::PARAM_INT;
    }

    return db_execute_sql($sql, $param, $paramType, $countRowSql, $countRowParam, $countRowParamType);
}

function db_get_radio_by_id($id) {
    $sql = 'select * from `Radio`.`myradio` where `myId` = :myId';
    $param = array(':myId' => $id);
    $paramType = array(':myId' => PDO::PARAM_INT);
    return db_execute_sql($sql, $param, $paramType);
}

function db_insert_radio($radioName, $radioURL, $myGenre, $myCountry, $myCity, $radioFormat, $myBitrate) {
    $sql = 'insert into `Radio`.`myradio` ' .
        '(`radioName`, `radioURL`, `myGenre`, `myCountry`, `myCity`, `myLocation`, `radioFormat`, `myBitrate`, `isEnable`) '.
        ' values ' .
        '(:radioName, :radioURL, :myGenre, :myCountry, :myCity, :myLocation, :radioFormat, :myBitrate, "1")';
    $param = [];
    $paramType = [];
    error_log($radioName);
    $param['radioName'] = $radioName;
    $param['radioURL'] = $radioURL;
    $param['myGenre'] = $myGenre;
    $param['myCountry'] = $myCountry;
    $param['myCity'] = $myCity;
    $param['myLocation'] = $myCountry . ' ' . $myCity;
    $param['radioFormat'] = $radioFormat;
    $param['myBitrate'] = $myBitrate;
    $paramType['myBitrate'] = PDO::PARAM_INT;
    error_log('b4 execute_insert!' . $radioName);
    return db_execute_update($sql, $param, $paramType);
}

function db_update_radio($myID, $radioName, $radioURL, $myGenre, $myCountry, $myCity, $radioFormat, $myBitrate, $isEnable) {
    $sql = 'update `Radio`.`myradio` set ' .
        '`radioName` = :radioName, ' .
        '`radioURL` = :radioURL, ' .
        '`myGenre` = :myGenre, ' .
        '`myCountry` = :myCountry, ' .
        '`myCity` = :myCity, ' .
        '`myLocation` = :myLocation, ' .
        '`radioFormat` = :radioFormat, ' .
        '`myBitrate` = :myBitrate, ' .
        '`isEnable` = :isEnable ' .
        'where `myID` = :myID';
    $param = [];
    $paramType = [];
    $param['radioName'] = $radioName;
    $param['radioURL'] = $radioURL;
    $param['myGenre'] = $myGenre;
    $param['myCountry'] = $myCountry;
    $param['myCity'] = $myCity;
    $param['myLocation'] = $myCountry . ' ' . $myCity;
    $param['radioFormat'] = $radioFormat;
    $param['myBitrate'] = $myBitrate;
    $param['myID'] = $myID;
    $param['isEnable'] = $isEnable;
    error_log('b4 execute_update!');
    return db_execute_update($sql, $param, $paramType);
}

function db_delete_radio($myID) {
    $sql = 'delete from `Radio`.`myradio` where `myID` = :myID';
    $param = array('myID' => $myID);
    return db_execute_update($sql, $param, array());
}

function db_get_all_preset() {
    $sql = 'select * from `Radio`.`preset`';
    return db_execute_sql($sql, array(), array());
}

function db_get_preset_by_id($id) {
    $sql = 'select * from `Radio`.`preset` where id = :id';
    $param = array('id' => $id);
    $paramType = array('id' => PDO::PARAM_INT);
    return db_execute_sql($sql, $param, $paramType);
}

function db_update_preset($id, $name, $url) {
    $sql = 'update `Radio`.`preset` set name = :name, url = :url where id = :id';
    $param = array('id' => $id, 'name' => $name, 'url' => $url);
    $paramType = array('id' => PDO::PARAM_INT);
    return db_execute_update($sql, $param, $paramType);
}

function db_get_radio_sort_by_location() {
    $sql = 'select `myCountry`, `radioName`, `radioURL`, `myBitrate`, `radioFormat`, `myGenre`, `myCity` from `Radio`.`myradio` where isEnable = \'1\' order by myCountry, myCity, radioName';
    return db_execute_sql($sql, array(), array());
}

function db_get_radio_genre() {
    $sql = 'select distinct `myGenre` from `Radio`.`myradio`';
    return db_execute_sql($sql, array(), array());
}

function db_get_radio_by_genre($genre) {
    $sql = 'select `myCountry`, `radioName`, `radioURL`, `myBitrate`, `radioFormat`, `myGenre`, `myCity` from `Radio`.`myradio` where isEnable = \'1\' and myGenre like :genre order by myCountry, myCity, radioName';
    $param = array('genre' => '%@' . $genre . '@%');
    return db_execute_sql($sql, $param, array());
}

function db_get_last_update($type) {
    $sql = 'select `last_update` from `Radio`.`last_update` where `type` = :type';
    return db_execute_sql($sql, array('type' => $type), array());
}

function db_get_test_count() {
    $sql = 'select status, count(*) as `count` from `Radio`.`test_run` group by `status`';
    return db_execute_sql($sql, array(), array());
}

function db_disable_offline_radio($count) {
    $sql = 'update `Radio`.`myradio` set `isEnable` = 0 where `offlineCount` >= :offline_count';
    $param = array('offline_count' => $count);
    $paramType = array('offline_count' => PDO::PARAM_INT);
    return db_execute_sql($sql, $param, $paramType);
}

function db_get_tester_config() {
    $sql = 'select `key`, `value` from `Radio`.`tester_config`';
    return db_execute_sql($sql, array(), array());
}

function db_get_tester_config_by_key($key) {
    $sql = 'select `key`, `value` from `Radio`.`tester_config` where `key` = :key';
    return db_execute_sql($sql, array('key' => $key), array());
}

function db_set_tester_config($config) {
    $sql = 'update `Radio`.`tester_config` set `value` = :input_value where `key` = :input_key';
    $result = '';
    foreach (array_keys($config) as $key) {
        $param = array('input_value' => $config[$key], 'input_key' => $key);
        $result = db_execute_sql($sql, $param, array());
    }
    return $result;
}

function db_start_tester() {
    $sql = 'update `Radio`.`tester_config` set `value` = null where `key` = "command"';
    $result = db_execute_sql($sql, array(), array());
    $sql = 'update `Radio`.`tester_config` set `value` = "STOPPED" where `key` = "status"';
    $result = db_execute_sql($sql, array(), array());
    $sql = 'update `Radio`.`tester_config` set `value` = now() where `key` = "next_test"';
    $result = db_execute_sql($sql, array(), array());
    return $result;
}

function db_toggle_tester_pause() {
    $sql = 'update `Radio`.`tester_config` set `value` = if(`value` = "PAUSE", null, "PAUSE") where `key` = "command"';
    $result = db_execute_sql($sql, array(), array());
    return $result;
}

function db_stop_tester() {
    $sql = 'update `Radio`.`tester_config` set `value` = "STOP" where `key` = "command"';
    $result = db_execute_sql($sql, array(), array());
    return $result;
}

?>
