PresetCollection = Backbone.Collection.extend({
    model: Preset,
    url: 'api/preset/',
    parse: function(response) {
        return response.data;
    }
});
